# Sample CI/CD Java 
 

 Exemplo de pipeline de desenvolvimento de um projeto Java/SpringBoot usando:
 * [CI/CD - GitLab](https://docs.gitlab.com/ee/ci/yaml/)
 * [Maven](https://maven.apache.org/)
 * [Versionamento semântico](https://semver.org/lang/pt-BR/) 
 * [Fluxo GitFlow](https://nvie.com/posts/a-successful-git-branching-model/)
 * [Containers Docker](https://www.docker.com/resources/what-container)
 * [Heroku](https://id.heroku.com/login/)

## Premissas:

* Ter o arquivo  .m2/settings.xml configurado na raiz do projeto. 
* Ter o arquivo Dockerfile na raiz do projeto. 
* Ter o ambiente/stack/serviço criado no Heroku.
* Ter as seguintes variaveis de ambiente configuradas no GitLab:
  * CI_GIT_URL - *ex: gitlab.com*  
  * CI_GIT_USER_NAME - *ex: git* 
  * CI_GIT_USER_PASS - *ex: 123456*
  * CI_REGISTRY_URL - *ex: registry.gitlab.com*
  * CI_REGISTRY_USER - *ex: docker*
  * CI_REGISTRY_PASSWORD - *ex: 123456*
  * DOCKER_AUTH_CONFIG - *ex: { "auths": { "registry.gitlab.com": {"auth": "ZG9ja2VyOmNqOVVpYTBRdzZhYjJkWU5lakVhcW9Ob0YyWUNBdXpkUll2RWpMZDFzb3c="} } }*
